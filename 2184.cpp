#include <iostream>
#include <set>
#include <vector>
#include <array>
#include <algorithm>
#include <iterator>
using namespace std;

class Solution {
public:
	double atlantis(const vector<array<double, 4>> & A) {
		vector<Line> X;
		for (const auto & i : A) {
			X.push_back(Line(i[0], i[1], i[3], -1));
			X.push_back(Line(i[2], i[1], i[3], 1));
		}
		sort(begin(X), end(X));
		multiset<array<double, 2>> Y;
		double result = 0;
		for (int n = X.size(), i = 0; i + 1 < n; ++i) {
			array<double, 2> e({X[i].y1, X[i].y2});
			if (X[i].z < 0) {
				Y.insert(e);
			}
			else {
				Y.erase(Y.find(e));
			}
			double dx = X[i + 1].x - X[i].x, dy = g(Y);
			result += dx * dy;
		}
		return result;
	}
private:
	struct Line {
		double x, y1, y2;
		int z;
		Line(double x, double y1, double y2, int z) {
			this->x = x;
			this->y1 = y1;
			this->y2 = y2;
			this->z = z;
		}
		bool operator< (const Line & other) const {
			if (this->x == other.x) {
				if (this->z == other.z) {
					if (this->z < 0) {
						return this->y1 < other.y1;
					}
					return this->y1 > other.y1;
				}
				return this->z < other.z;
			}
			return this->x < other.x;
		}
	};
	double g(const multiset<array<double, 2>> & Y) {
		double result = 0;
		for (multiset<array<double, 2>>::iterator it = begin(Y), jt = end(Y); it != jt; ++it) {
			double a = (*it)[0], b = (*it)[1];
			while (next(it) != jt and (*next(it))[0] <= b) {
				b = max(b, (*next(it))[1]);
				++it;
			}
			result += b - a;
		}
		return result;
	}
};

int main(void) {
	Solution solution;
	int N, k = 0;
	while (cin >> N and N > 0) {
		vector<array<double, 4>> A;
		for (int i = 0; i < N; ++i) {
			array<double, 4> e;
			cin >> e[0] >> e[1] >> e[2] >> e[3];
			A.push_back(e);
		}
		cout.precision(2);
		cout << fixed;
		cout << "Test case #" << ++k << '\n';
		cout << "Total explored area: " << solution.atlantis(A) << "\n\n";
	}
	return 0;
}