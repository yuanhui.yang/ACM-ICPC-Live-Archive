# ACM-ICPC-Live-Archive

| # | Title | Solution |
| --- | --- | --- |
| [2184](https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=8&category=68&page=show_problem&problem=185) | [Atlantis](https://github.com/yuanhui-yang/ACM-ICPC-Live-Archive/blob/master/2184.pdf) | [C++](https://github.com/yuanhui-yang/ACM-ICPC-Live-Archive/blob/master/2184.cpp) |